from machine import Pin, PWM
from utime import sleep_ms
import math


class LED(object):
	def __init__(self, pin=None):
		self.__led = PWM(Pin(pin), freq=1000, duty=0)

		print("current: freq: {}, duty: {}".format(self.__led.freq(), self.__led.duty()))
	
	def pulse(self, time):
		for i in range(20):
			duty = int(math.sin(i / 10 * math.pi) * 500 + 500)
			# print(duty)
			self.__led.duty(duty)
			sleep_ms(time)

	def test(self, duty, time):
		# self.__led.freq(freq)
		self.__led.duty(duty)

		sleep_ms(time)


def main():
	led = LED(18)
	duty = 0
	step = 20

	while True:
		# led.pulse(50)
		time = 200 if duty == 0 else 50
		led.test(duty, time)

		duty += step

		if duty == 0 or duty >= 500:
			step = -step


if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")
  