from utime import sleep_ms
from keyboard.keypad_matrix import KeyPad
from led.ledpad_matrix import LedPad


class UnitTest(object):
	def __init__(self):
		self.__keypad = None
		self.__ledpad = None

		self.__init_keypad()
		self.__init_ledpad()

	def __key_down_cb(self, row, column):
		self.__ledpad.light_on(row, column)

	def __key_up_cb(self, row, column):
		self.__ledpad.light_off(row, column)

	def __init_keypad(self):
		ROW_SET = (15, 2, 4, 5, 18) # for output
		COLUMN_SET = (19, 21, 22, 23) # for intput
		PIN_SET = (ROW_SET, COLUMN_SET)

		self.__keypad = KeyPad(PIN_SET, self.__key_down_cb, self.__key_up_cb)
		print("Keypad key count:", self.__keypad.get_key_count())
	
	def __init_ledpad(self):
		ROW_SET = (13, 12, 14, 27, 26) # for output
		COLUMN_SET = (25, 33, 32) # for intput
		PIN_SET = (ROW_SET, COLUMN_SET)

		self.__ledpad = LedPad(PIN_SET)
		print("LedPad led count:", self.__ledpad.get_led_count())

	def start(self):
		self.__keypad.capture()


def main():
	tester = UnitTest()
	tester.start()


if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")
