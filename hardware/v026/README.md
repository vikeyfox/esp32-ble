### Keypad 验证板 v0.2.6 硬件介绍

由于还不会画 ESP32 芯片的外围电路和充电电路，所以`v0.1`版本使用直插`ESP32 DevKit v1`进行有线供电的方案

同时为了避免浪费，所以`PCB 板`尺寸控制在 10 * 10 厘米以内（已经打板，希望这次验证成功）

> `ESP32 DevKit v1` 是这种 30 针的开发板

> ![](https://gitee.com/walkline/ESP32-BLE-Mechanical_Keypad/raw/master/res/images/esp32_devit_v1.png)

### 所需硬件列表

* `ESP32 DevKit v1 开发板` * 1
* `按键 (6*6*5)` * 24
* `贴片二极管 (1N4148WS)` * 24
* `PCB 板` * 1
	* 下载 [生产文件](https://gitee.com/walkline/walkline-wardware/raw/master/keypad/matrix/v0.2.6/Gerber_5_5%20matrix%20keypad%20v0.2.6_20200409134715.zip)

### 失败原因

之前几次之所以失败就是因为选错了`GPIO 引脚`，这次必须要记录下来了！

| 引脚 | 失败原因 | 建议 |
| :-: | :-: | :-: |
| 36 (VP) | 作为输入引脚无法拉高，状态飘忽不定 | 不使用 |
| 39 (VN) | 同上 | 不使用 |
| 34 | 同上 | 不使用 |
| 35 | 同上 | 不使用 |
| 5 | 仅支持输出 | 无 |
| 2 | 板载 Led 控制 | 少用 |
| 1 (TX0) | 开发调试使用 | 不使用 |
| 3 (RX0) | 开发调试使用 | 不使用 |

### GPIO 引脚定义

* `按键`使用 2 组 GPIO 做行列扫描

	* `k_col`为列线，输入
	* `k_row`为行线，输出

* `Led`使用 1 组 GPIO 做行扫描，列线接地
* `Led 控制`使用 4 个 GPIO 单独控制功能指示灯

| 按键定义 | 引脚 | Led 定义 | 引脚 | Led 控制 | 引脚 |
| :-: | :-: | :-: | :-: | :-: | :-: |
| k_col_1 | 32 | l_row_1 | 21 | l_control_1 | 17 |
| k_col_2 | 33 | l_row_2 | 19 | l_control_2 | 16 |
| k_col_3 | 25 | l_row_3 | 18 | l_control_3 | 4 |
| k_col_4 | 26 | l_row_4 | 5 | l_control_4 | 15 |
| k_col_5 | 27 | l_row_4 | | | |
| k_row_1 | 14 | | | |
| k_row_2 | 12 | | | |
| k_row_3 | 13 | | | |
| k_row_4 | 23 | | | |
| k_row_5 | 22 | | | |

### 按键矩阵扫描

![](../../images/v0.2.6/scheme.png)

#### 方法一：行输出

列线作为输入默认全部`拉低`，行线轮流`置高电平`并逐个检查列线输入状态

#### 方法二：列输出

二极管全部反向安装，列线作为输入默认全部`拉高`，行线轮流`置低电平`并逐个检查列线输入状态

### 层定义

既然是自己开发的控制器，那么当然可以自由定义每一层如何应用

> 层，是我看来的概念，大概意思就是每一层的同一个按键可以对应不同的功能

目前只想到了两层的定义

* Layer 01

	| 定义 1 | 定义 2 |
	| :-: | :-: |
	| ![](../../images/layout/5x5_keypad_layout_01_01.png) | ![](../../images/layout/5x5_keypad_layout_01_02.png) |

* Layer 04

	* `Light`用于切换不同灯效，目前想到但还没实现的效果有：

		* 灯全灭
		* 灯全亮
		* 呼吸灯
		* 按键按下整行闪烁

	* `Bright`用于调节灯光亮度

	* `Test`用于测试整行效果

	| 定义 |
	| :-: |
	| ![](../../images/layout/5x5_keypad_layout_04.png) |

### PCB 电路板

这次做一个异形板挖哈哈

#### 效果图

| 正面 | 背面 |
| :-: | :-: |
| ![](../../images/v0.2.6/front.png) | ![](../../images/v0.2.6/back.png) |

#### 实物图

| 正面 | 背面 |
| :-: | :-: |
| ![](../../images/v0.2.6/photo/front.png) | ![](../../images/v0.2.6/photo/back.png) |

#### 完成图

| 正面 |
| :-: |
| ![](../../images/v0.2.6/photo/finish.png) |
