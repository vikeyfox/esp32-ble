### Keypad 验证板 v0.2.5 硬件介绍

由于还不会画 ESP32 芯片的外围电路和充电电路，所以`v0.1`版本使用直插`ESP32 DevKit v1`进行有线供电的方案

同时为了避免浪费，所以`PCB 板`尺寸控制在 10 * 10 厘米以内（已经打板，~~希望这次验证成功~~，废了）

> `ESP32 DevKit v1` 是这种 30 针的开发板

> ![](https://gitee.com/walkline/ESP32-BLE-Mechanical_Keypad/raw/master/res/images/esp32_devit_v1.png)

### 所需硬件列表

* `ESP32 DevKit v1 开发板` * 1
* `按键 (6*6*5)` * 24
* `贴片二极管 (1N4148WS)` * 24
* `PCB 板` * 1
	* 下载 [生产文件](https://gitee.com/walkline/walkline-wardware/raw/master/keypad/matrix/v0.2.5/Gerber_5_5%20matrix%20keypad%20v0.2.5_20200401095731.zip)

### 层定义

既然是自己开发的控制器，那么当然可以自由定义每一层如何应用

> 层，是我看来的概念，大概意思就是每一层的同一个按键可以对应不同的功能

目前只想到了两层的定义

* Layer 01

	| 定义 1 | 定义 2 |
	| :-: | :-: |
	| ![](../../images/layout/5x5_keypad_layout_01_01.png) | ![](../../images/layout/5x5_keypad_layout_01_02.png) |

* Layer 04

	* `Light`用于切换不同灯效，目前想到但还没实现的效果有：

		* 灯全灭
		* 灯全亮
		* 呼吸灯
		* 按键按下整行闪烁

	* `Bright`用于调节灯光亮度

	* `Test`用于测试整行效果

	| 定义 |
	| :-: |
	| ![](../../images/layout/5x5_keypad_layout_04.png) |

### PCB 电路板

#### 效果图

| 正面 | 背面 |
| :-: | :-: |
| ![](../../images/v0.2.5/front.png) | ![](../../images/v0.2.5/back.png) |

#### 实物图

| 正面 | 背面 |
| :-: | :-: |
| ![](../../images/v0.2.5/photo/front.png) | ![](../../images/v0.2.5/photo/back.png) |

#### 完成图

| 正面 |
| :-: |
| ![](../../images/v0.2.5/photo/finish.png) |
