<h1 align="center">ESP32 BLE</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

MicroPython for ESP32 开发板低功耗蓝牙（BLE）研究学习项目

### 名词解释

写的比较乱，所以需要规范以下名词解释

* BLE：低功耗蓝牙设备（Bluetooth Low Energy、Bluetooth LE）
* Peripheral、BLE 设备、外围设备、设备：代表 `ESP32 开发板`
* Central、中心设备：代表`手机、电脑等`可以连接蓝牙外设的设备

### 制定目标

使用 BLE 实现蓝牙键盘控制器功能，也就是自制蓝牙（小）键盘（HID）

要自制（小）蓝牙键盘，需要完成下列功能：

* 开发板实现 HID 功能，即作为外围设备，可以被中心设备扫描连接
* 制作（小）键盘硬件，编写键盘驱动，实现通过开发板向中心设备输入内容
* 硬件还包括灯光效果，用一个按键进行灯效切换：
	* 全灭（默认）
	* 全亮
	* 全部呼吸闪烁
	* ~~按键点击闪烁~~
* 设计字符映射表统一规范，方便对应按键和输入内容

### 进度情况

* [视频演示](https://www.bilibili.com/video/av91982504/)
* 已完成开发板作为外围设备的功能，可以被手机扫描连接并简单输入一些字母和数字
* 已完成（小）键盘硬件电路设计，等待合适时机打板制作，目前有 2 种方案
* 打了若干次板子但是验证功能均失败了，目前硬件已经更新到`v0.2.5`，准备打板继续验证
* 目前[`v0.2.6`](https://gitee.com/walkline/esp32-ble/tree/master/hardware/v026)按键部分硬件已经验证成功，终于可以继续后续开发了。。。

### 已知问题

* ~~BLE 设备在第一次绑定连接成功后，处于连接状态时，复位设备，或者关闭再打开`中心设备`的蓝牙功能后，设备会处于已连接状态，但是`中心设备`没有任何反应，既不会连接成功也不会连接失败，此处怀疑是因为缺少连接超时设置，但是当前版本的设备固件并不支持设置超时~~

* ~~在 Windows 10 系统上可以搜索到 BLE 设备并进行连接，但是很快会出现`驱动程序错误`的提示，暂时无解~~

* 导致以上已知问题的原因为，`MicroPython ubluetooth`**不支持**设备间的配对和绑定操作，这个问题只能等

* 自己尝试给固件增加配对功能，目前在`iOS`上测试成功了

### 分支项目

在等待打板的这段时间里又继续研究了 BLE 的其它玩法，总结一下：

* [MicroPython Beacon Library](https://gitee.com/walkline/micropython-beacon-library)：这个库适用于实现包括谷歌的`Eddystone`和苹果的`iBeacon`在内的 2 种信标，还包括了不是很精确的信标测距功能
* [ESP32 BLE - Remote Controller](https://gitee.com/walkline/ESP32-BLE-Remote_Controller)：这个项目用于制作蓝牙自拍按钮，可惜目前还不支持苹果的设备
* [ESP32 BLE - MI Temperature Humidity 2 Reader](https://gitee.com/walkline/ESP32-BLE-MI_Temperature_Humidity_2_Reader)：这个就有点无聊了，用于读取`小米温湿度计2`的温湿度数据

* [MicroPython BLE Library](https://gitee.com/walkline/micropython-ble-library)：根据之前研究学习到的 Micropython BLE 相关知识，将相关代码模块封装成为类库，方便以后使用

* [ESP32 BLE - 机械小键盘](https://gitee.com/walkline/ESP32-BLE-Mechanical_Keypad)：重点项目，制作机械轴小键盘

* [ESP32 BLE - UART](https://gitee.com/walkline/esp32-ble-uart)：使用`UART`与其它设备交换数据

### BLE 知识

> 非业内人士，以下内容纯属个人理解，如有偏差欢迎斧正

* BLE 设备遵循 HOGP 规范（HID over GATT Profile）
* `Profile`包扩很多`Services`
* `Services`包扩很多`Characteristics`
* `Characteristics`包扩很多`Descriptors`

	例如蓝牙键盘就是一个`Profile`，它包括了至少（**必要的**）3 个`Services`：

	* `Battery Service`
	* `Device Information`
	* `Human Interface Device`

	`Battery Service`又包括了 1 个`Characteristics`：

	* `Battery Level`

	`Battery Level`还可以包括 1 个`Descriptors`：

	* `Client Characteristic Configuration`

### 软件部分

首先参考`HOGP 规范`，配置好所有的`Services`、`Characteristics`和`Descriptors`

BLE 设备作为`外围设备（GATT Server）`，本地注册所有`Services`，生成`payload`，等待`中心设备（GATT Client）`连接

然后作为`广播者`将`payload`进行广播，包括：

* 本地名称（显示名称）
* 所有的`Services`
* 外观（显示名称旁边的图标）

`中心设备`扫描到广播后进行内容分析、展示，用户点击展示内容后开始和`外围设备`进行绑定连接，并读取保存在 BLE 设备本地的`Characteristics`和`Descriptors`信息，最终完成连接，等待接收数据

此过程中我们只负责`外围设备（GATT Server）`的开发

### 硬件部分（按键）

已完成电路设计的（小）键盘 2 种方案，分别是：

* ADC 采样方案
* 键盘矩阵方案

#### ADC 采样方案

ADC 采样方案使用的是同型号电阻串联的方式，如图：

![ADC 原理图](images/screenshot_01.png)

优点：

* 占用 IO 口少，一个 IO 口至少可以采集 10 个按键输入（未验证）
* 电阻型号一致，方便管理购买

缺点：

* 电阻串联方案导致只有 1 个按键的优先级别最高，也就是同时只能检测到 1 个按键按下

这种方案能想到的用途有 2 个：

* 作为小键盘使用，因为小键盘不需要多个按键同时按下
* 使用 2 路 ADC，其中一路作为控制键（Ctrl、Alt、Shift等），配合另外一路进行简单输入

#### 键盘矩阵方案

键盘矩阵方案是常规键盘方案，连接方式如图：

![矩阵原理图](images/screenshot_02.png)

优点：

* 可以做到全键无冲
* 电路简单，几乎只用到按键

缺点：

* 占用 IO 口较多，`row * column`个按键需要`row + column`个 IO，例如`4 * 5`的 20 键键盘需要 9 个 IO

### 硬件部分（灯效）

灯效也将使用矩阵方案（待验证）并使用按键切换 4 种不同灯效

![矩阵原理图](images/screenshot_03.png)

### 相关链接

* [Walkline Hardware](https://gitee.com/walkline/walkline-wardware)：今后所有的开源硬件的`生产文件`和`BOM 表`都会放在这个项目中，包括本项目用到的 3 种方案

* [ESP32 自定义固件下载](https://gitee.com/walkline/esp32_firmware)

### 参考资料

* [MicroPython 官方文档](http://docs.micropython.org/en/latest/library/ubluetooth.html)

* [MicroPython 项目示例](https://github.com/micropython/micropython/tree/master/examples/bluetooth)

* Bluetooth GATT

	* [Services](https://www.bluetooth.com/specifications/gatt/services/)

		* [Human Interface Device](https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Services/org.bluetooth.service.human_interface_device.xml)

		* [Battery Service](https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Services/org.bluetooth.service.battery_service.xml)

		* [Device Information](https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Services/org.bluetooth.service.device_information.xml)

	* [Characteristics](https://www.bluetooth.com/specifications/gatt/characteristics/)

	* [Descriptors](https://www.bluetooth.com/specifications/gatt/descriptors/)

	* [Generic Attributes](https://www.bluetooth.com/specifications/gatt/)
		* [HID over GATT Profile.pdf](https://www.bluetooth.org/docman/handlers/downloaddoc.ashx?doc_id=245141)：HOGP 规范

	* [Appearance](https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.gap.appearance.xml)：蓝牙设备外观对应表

* [USB HID Usage Table](http://www.freebsddiary.org/APC/usb_hid_usages)：键盘按键键值对应表

* [HID Descriptor Tool](https://www.usb.org/document-library/hid-descriptor-tool)：用于生成`Report Map Data`

### 好文推荐

* [广播包解析（作者：强光手电）](https://www.cnblogs.com/aikm/p/5022502.html)
* [扫描请求和扫描响应（作者：强光手电）](https://www.cnblogs.com/aikm/p/5144209.html)

### 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
    * 走线物联：163271910
    * 扇贝物联：31324057

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>