"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/esp32-ble
"""
from hardware.v026.keypad import MyKeyPad


if __name__ == "__main__":
	try:
		keypad = MyKeyPad()
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")
