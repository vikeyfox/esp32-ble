"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/esp32-ble
"""
from .keycode import KeyCode

class KeyUtils(object):
	@staticmethod
	def is_control_key(keycode):
		return keycode in range(KeyCode.KC_LCTRL, KeyCode.KC_RGUI)

	@staticmethod
	def is_custom_control_key(keycode):
		return keycode in range(KeyCode.KCC_FN, KeyCode.KCC_BDOWN)
